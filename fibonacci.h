#include "matrix.h"
#include <algorithm>
#include <ctime>

class fibonacci {

	longlongplus result;

public:
	fibonacci() { }

	longlongplus recursive(int number) {
		if (number == 0)
			return longlongplus(0);

		if (number == 1)
			return longlongplus(1);

		return recursive(number - 1) + recursive(number - 2);
	}

	longlongplus iterations(int number) {
		longlongplus current(1);
		longlongplus temp(0);
		longlongplus next(1);

		if (number == 0)
			return longlongplus(0);

		if (number == 1)
			return longlongplus(1);

		for (unsigned iteration = 2; iteration <= number; iteration++) {
			temp = next;
			next = current + next;
			current = temp;
		}

		return current;
	}

	double bine(int number) {
		return (pow((1 + sqrt(5)) / 2, number) - pow((1 - sqrt(5)) / 2, number)) / sqrt(5);
	}

	pair<longlongplus, longlongplus> universal(int number) {
		matrix<longlongplus> middle(longlongplus(0), longlongplus(1), longlongplus(1), longlongplus(1));
		matrix<longlongplus> fib(longlongplus(0), longlongplus(1), true);
		matrix<longlongplus> result(longlongplus(0), longlongplus(1), longlongplus(1), longlongplus(1));

		for (int power = 1; power < number; power++) {
			result = result * middle;
		}

		auto f = fib * result;
		return make_pair(f.getElement(0, 0), f.getElement(0, 1));
	}

	pair<longlongplus, longlongplus> _dirac(int number) {
		if (number == 2)
			return make_pair(longlongplus(1), longlongplus(1));
		if (number == 1)
			return make_pair(longlongplus(1), longlongplus(0));
		if (number % 2 == 0) {
			auto stick = _dirac(number / 2);
			auto some = stick.first*stick.second;
			auto shit = stick.first*stick.first;
			return make_pair(shit + some + some, shit + stick.second*stick.second);
		}
		else {
			auto stick = _dirac(number - 1);
			return make_pair(stick.first + stick.second, stick.first);
		}
	}

	pair<longlongplus, longlongplus> dirac(int number=100) {
		auto numbersFib = _dirac(number);
		return numbersFib;
	}

	/*Тестирование методов*/
	void calcTime(pair<longlongplus, longlongplus>(*f)(int)) {
		vector<int> rnd(pow(10, 6));
		generate(rnd.begin(), rnd.end(), []{ return rand() % 50 + 51; });

		time_t start = time(NULL);
		longlongplus sum(0);
		for (auto number : rnd) {
			sum = sum + f(number).first;
		}
		time_t end = time(NULL);
		cout << "Time is " << end - start << endl;
	}
};