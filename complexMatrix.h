#pragma once
#include <vector>
#include <complex>

using namespace std;

class complexMatrix {

    public:

        vector<vector<complex<double> > > matrix;


        complexMatrix(int rows, int cols) {
            matrix.resize(rows);
            for (int i = 0; i < rows; i++) {
                matrix[i].resize(cols);
            }
        }

        complexMatrix(const vector<vector<complex<double> > > &_matrix) {
            matrix.resize(_matrix.size());
            for (int row = 0; row < _matrix.size(); row++) {
                matrix[row].resize(_matrix[0].size());
                for (int column = 0; column < _matrix[0].size(); column++) {
                    matrix[row][column] = _matrix[row][column];
                }
            }
        }

        complexMatrix(complex<double> init, int rows, int cols) {
            matrix.resize(rows);
            for (int i = 0; i < rows; i++) {
                matrix[i].resize(cols);
                for (int j = 0; j < cols; j++) {
                    matrix[i][j] = init;
                }
            }
        }

        complexMatrix() {}


        inline int getRows() const { return this->matrix.size(); }
        inline int getCols() const { return this->matrix[0].size(); }

        inline complex<double>& operator()(int row, int column) { return matrix[row][column]; }


        complexMatrix operator=(const complexMatrix &_matrix) {
            if (this == &_matrix)
                return *this;

            if (this->matrix.size() != _matrix.matrix.size() || this->matrix[0].size() != _matrix.matrix[0].size()) {
                this->matrix.resize(_matrix.matrix.size());
                for (int row = 0; row < _matrix.matrix.size(); row++) {
                    matrix[row].resize(_matrix.matrix[0].size());
                }
            }

            for (int row = 0; row < this->matrix.size(); row++) {
                for (int column = 0; column < this->matrix[0].size(); column++) {
                    this->matrix[row][column] = _matrix.matrix[row][column];
                }
            }

            return *this;
        }

        complexMatrix operator+=(const complexMatrix &_matrix) {
            for (int row = 0; row < _matrix.matrix.size(); row++) {
                for (int column = 0; column < _matrix.matrix.size(); column++) {
                    matrix[row][column] += _matrix.matrix[row][column];
                }
            }

            return *this;
        }

        complexMatrix operator-=(const complexMatrix &_matrix) {
            for (int row = 0; row < _matrix.matrix.size(); row++) {
                for (int column = 0; column < _matrix.matrix.size(); column++) {
                    matrix[row][column] -= _matrix.matrix[row][column];
                }
            }

            return *this;
        }

        complexMatrix operator*=(const complexMatrix &_matrix) {

            complexMatrix result(this->getRows(), _matrix.getCols());

            for (int row = 0; row < result.matrix.size(); row++) {
                for (int column = 0; column < result.matrix[0].size(); column++) {
                    for (int k = 0; k < this->matrix[0].size(); k++) {
                        result.matrix[row][column] = result.matrix[row][column] + this->matrix[row][k] * _matrix.matrix[k][column];
                    }
                }
            }

            this->matrix = result.matrix;

            return *this;
        }

        complexMatrix operator*=(double number) {

            for (int row = 0; row < this->getRows(); row++) {
                for (int column = 0; column < this->getCols(); column++) {
                    matrix[row][column] *= number;
                }
            }

            return *this;
        }

        complexMatrix operator*(const complexMatrix &_b) {
            complexMatrix result(this->getRows(), _b.getCols());

            for (int row = 0; row < result.matrix.size(); row++) {
                for (int column = 0; column < result.matrix[0].size(); column++) {
                    for (int k = 0; k < this->matrix[0].size(); k++) {
                        result.matrix[row][column] = result.matrix[row][column] + this->matrix[row][k] * _b.matrix[k][column];
                    }
                }
            }

            return result;
        }

        complexMatrix operator/=(double number) {

            for (int row = 0; row < this->getRows(); row++) {
                for (int column = 0; column < this->getCols(); column++) {
                    matrix[row][column] /= number;
                }
            }

            return *this;
        }

        complexMatrix operator^(int power) {

            complexMatrix m(this->matrix);
            complexMatrix result(this->matrix);
            for (int i = 0; i < power - 1; i++) {
                result = m * result;
            }

            return result;
        }

        /*
        ** Получение строки матрицы как горизонтальную матрицу
        */
        complexMatrix getRow(int number = 0) {
            complexMatrix result(1, this->getCols() - 1);

            for (int col = 1; col < this->getCols(); col++) {
                result.matrix[0][col-1] = this->matrix[number][col];
            }

            return result;
        }

        /*
        ** Получение столбца матрицы как вертикальную матрицу
        */
        complexMatrix getColumn(int number = 0) {
            complexMatrix result(this->getRows() - 1, 1);

            for (int row = 1; row < this->getRows(); row++) {
                result.matrix[row-1][0] = this->matrix[row][number];
            }

            return result;
        }

        /*
        ** Получение подматрицы размером (n-1)x(n-1)
        */
        complexMatrix subMatrix() {

            if (!this->matrix.size()) {
                return complexMatrix();
            }

            complexMatrix result(this->getRows() - 1, this->getCols() - 1);
            for (int row = 1; row < this->getRows(); row++) {
                for (int col = 1; col < this->getCols(); col++) {
                    result.matrix[row-1][col-1] = this->matrix[row][col];
                }
                cout << endl;
            }
            return result;
        }

        /*
        ** Получение первого элемента матрицы
        */
        complex<double> getFirstElement() {
            return this->matrix[0][0];
        }

        /*
        ** Генератор матрицы T
        */
        complexMatrix getMatrixT() {
            complexMatrix result(this->getRows() + 1, this->getCols());

            complexMatrix r = this->getRow();
            complexMatrix c = this->getColumn();
            complex<double> a = this->getFirstElement();
            complexMatrix subA = this->subMatrix();

            /*for (int i = 0; i < subA.getRows(); i++) {
                for (int j = 0; j < subA.getCols(); j++) {
                    cout << subA.matrix[i][j] << " ";
                }
                cout << endl;
            }*/

            for (int row = 0; row < this->getRows() + 1; row++) {
                for (int col = 0; col < this->getCols(); col++) {
                    if (row == col) {
                        result.matrix[row][col] = 1;
                        continue;
                    }

                    if (row == col + 1) {
                        result.matrix[row][col] = -a;
                        continue;
                    } 
                    
                    if (row > col + 1) {
                        int power = row - 2 - col;

                        complexMatrix temp;
                        if (power == 0) {
                            temp = r * c;
                        } else {
                            complexMatrix subPowerA = subA^power;
                            temp = r * subPowerA;
                            temp = temp * c;
                        }
                        /*cout << "RA^" << power << "C:" << endl;
                        for (int i = 0; i < temp.getRows(); i++) {
                            for (int j = 0; j < temp.getCols(); j++) {
                                cout << temp.matrix[i][j] << " ";
                            }
                            cout << endl;
                        }*/

                        complex<double> negative = -1;
                        result.matrix[row][col] = negative * temp.matrix[0][0];
                    }
                    //cout << result.matrix[row][col] << " ";
                }
                
                //cout << endl;
            }
            return result;
        }
};

complexMatrix operator+(const complexMatrix &_a, const complexMatrix &_b) {
    complexMatrix result(_a);
    return (result += _b);
}

complexMatrix operator-(const complexMatrix &_a, const complexMatrix &_b) {
    complexMatrix result(_a);
    return (result -= _b);
}

complexMatrix operator*(const complexMatrix &_a, const complexMatrix &_b) {
    complexMatrix result(_a.getRows(), _b.getCols());

    for (int row = 0; row < result.matrix.size(); row++) {
        for (int column = 0; column < result.matrix[0].size(); column++) {
            for (int k = 0; k < _a.matrix[0].size(); k++) {
                result.matrix[row][column] = result.matrix[row][column] + _a.matrix[row][k] * _b.matrix[k][column];
            }
        }
    }

    return result;
}

complexMatrix operator*(const complexMatrix &_a, double number) {
    complexMatrix result(_a);
    return (result *= number);
}

complexMatrix operator/(const complexMatrix &_a, double number) {
    complexMatrix result(_a);
    return (result /= number);
}

ostream& operator<<(ostream &out, const complexMatrix &_matrix) {
    for (int row = 0; row < _matrix.getRows(); row++) {
        for (int column = 0; column < _matrix.getCols(); column++) {
            out << " " << _matrix.matrix[row][column];
        }
        out << endl;
    }
    return out;
}

istream& operator>>(istream &in, complexMatrix &_matrix) {
    for (int row = 0; row < _matrix.getRows(); row++) {
        for (int column = 0; column < _matrix.getCols(); column++) {
            in >> _matrix.matrix[row][column];
        }
    }
    return in;
}