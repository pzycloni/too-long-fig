#include <iostream>
#include "complexMatrix.h"
#include "polynomial.h"

using namespace std;


polynomial<complex<double> > generateSelfValues(complexMatrix m) {
	
	polynomial<complex<double> > result;
	complexMatrix temp = m;

	vector<complexMatrix> containerSubMatrix;
	containerSubMatrix.push_back(m);

	vector<complexMatrix> containerMatrixT;

	for (int i = 0; i < m.getRows(); i++) {
		containerMatrixT.push_back(containerSubMatrix[i].getMatrixT());
		containerSubMatrix.push_back(containerSubMatrix[i].subMatrix());
	}

	for (int i = 1; i < containerMatrixT.size(); i++) {
		containerMatrixT[0] = containerMatrixT[0] * containerMatrixT[i];
	}

	for (int i = containerMatrixT[0].getRows() - 1; i >= 0; i--) {
		result.coefficients.push_back(containerMatrixT[0].matrix[i][0]);
	}

	return result;
}


int main() {
	
	/*vector<complex<double> > v;
	v.push_back(-3);
	v.push_back(4);
	v.push_back(0);
	v.push_back(1);
	polynomial<complex<double> > p(v);

		vector<pair<int, complex<double> > > roots = p.findDegreeMultipleRoot(p);
		for (int i = 0; i < roots.size(); i++) {
			cout << "Корень " << roots[i].second << " кратности " << roots[i].first << endl;
		}
		
	*/

	int N = 3;
	vector<vector<complex<double> > > m(N, vector<complex<double> >(N));

	/*m[0][0] = 14;
	m[0][1] = -4;
	m[0][2] = 0;
	
	m[1][0] = -4;
	m[1][1] = 12;
	m[1][2] = -4;

	m[2][0] = 0;
	m[2][1] = -4;
	m[2][2] = 10;*/


	m[0][0] = 1;
	m[0][1] = 2;
	m[0][2] = 3;
	
	m[1][0] = 4;
	m[1][1] = 5;
	m[1][2] = 6;

	m[2][0] = 7;
	m[2][1] = 8;
	m[2][2] = 9;

	complexMatrix v(m);

	cout << v << endl;

	// поиск хар-го мн-на
	polynomial<complex<double> > p = generateSelfValues(v);
	cout << p << endl;
	// поиск корня мн-на
	complex<double> root = p.findRoot();
	cout << root << endl;

	return 0;
}