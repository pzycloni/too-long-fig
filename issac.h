#include <math.h>
#include <complex>

class issac {

	double precision;

public:
	issac(double precision) { this->precision = precision; }

	pair<long double, long double> method(double argument, double (*function)(double), double (*derivative)(double)) {
		double prevArgument = argument;

		while (abs((*function)(argument)) > this->precision) {
			argument = prevArgument - (*function)(prevArgument) / (*derivative)(prevArgument);
			
			prevArgument = argument;
		}
		
		return make_pair(argument, (*function)(argument));
	}

	long long int _factorial(int number) {
		long long int result = 1;
		for (int iteration = 1; iteration <= number; iteration++) {
			result *= iteration;
		}
		return result;
	}

	double _Cnk(unsigned k, unsigned n) {
		return this->_factorial(n) / (this->_factorial(k)*(this->_factorial(n-k)));
	}

	double _binom(pair<double, double> ab, int power) {
		long double result = 0;
		for (int iteration = 0; iteration <= power; iteration++) {
			result += this->_Cnk(iteration, power) * pow(ab.first, power - iteration) * pow(ab.second, iteration);
		}
		return result;
	}

	vector <double> coefficients(int power) {
		// что то тут не так!
	}
};