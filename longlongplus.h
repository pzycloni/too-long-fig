#include <string>
#include <vector>

using namespace std;

const unsigned long long int LONG = 10e15;

class longlongplus {
	unsigned long long low, middle, high;

public:
	longlongplus(unsigned long long number = 0) {
		low = number % LONG;
		middle = (number / LONG) % LONG;
		high = (number / LONG) / LONG;
	}
	longlongplus(unsigned long long _low, unsigned long long _middle, unsigned long long _high) {
		low = _low;
		middle = _middle;
		high = _high;
	}

	longlongplus operator+(const longlongplus &summand) const {
		return longlongplus((summand.low + low) % LONG, (summand.middle + middle + (summand.low + low) / LONG) % LONG, (summand.high + high) + (summand.middle + middle + (summand.low + low) / LONG) / LONG);
	}

	longlongplus operator+=(const longlongplus &summand) const {
		return longlongplus((summand.low + low) % LONG, (summand.middle + middle + (summand.low + low) / LONG) % LONG, (summand.high + high) + (summand.middle + middle + (summand.low + low) / LONG) / LONG);
	}

	longlongplus operator*(const longlongplus &multiplicand) const {
		string first = to_string(multiplicand.high) + to_string(multiplicand.middle) + to_string(multiplicand.low);
		string second = to_string(this->high) + to_string(this->middle) + to_string(this->low);

		vector <string> lines;
		for (int i = second.length() - 1; i >= 0; i--) {
			lines.push_back("");
			int store = 0;
			for (int j = first.length() - 1; j >= 0; j--) {
				int current = ((int)second[i] - 48)*((int)first[j] - 48) + store;
				lines[(second.length() - 1) - i] = to_string(current % 10) + lines[(second.length() - 1) - i];
				store = current / 10;
			}
			if (store != 0)
				lines[(second.length() - 1) - i] = to_string(store) + lines[(second.length() - 1) - i];
		}
		longlongplus result(0);
		for (int i = 0; i < lines.size(); i++) {
			if (i >= 0){
				for (int j = i; j > 0; j--) {
					lines[i] += "0";
				}
				if (lines[i].length() > 15) {

					int limit = 15;

					int lowIndex = lines[i].size() - limit;
					int lowCount = limit;

					int highIndex = 0;
					int highCount = 0;

					int middleIndex = lines[i].size() - limit * 2;
					int middleCount = limit;

					if (middleIndex <= 0) {
						middleCount += middleIndex;
						middleIndex = 0;
						highIndex = 0;
					} else {
						highCount = lines[i].size() - limit * 2;
					}
					

					string lowTemp = lines[i].substr(lowIndex, lowCount);
					string middleTemp = lines[i].substr(middleIndex, middleCount);
					string highTemp = lines[i].substr(0, highCount);
					
					if (middleTemp == "")
						middleTemp = "0";

					if (highTemp == "")
						highTemp = "0";
					

					result = longlongplus(stoull(lowTemp), stoull(middleTemp), stoull(highTemp)) + result;
				}
				else {
					result = longlongplus(stoull(lines[i])) + result;
				}
			}
		}
		return result;
	}

	friend string to_string(const longlongplus &lpnum) {
		if (lpnum.high == 0 && lpnum.middle == 0)
			return std::to_string(lpnum.low);
		if (lpnum.high == 0)
			return std::to_string(lpnum.middle) + std::to_string(lpnum.low);
		return std::to_string(lpnum.high) + std::to_string(lpnum.middle) + std::to_string(lpnum.low);
	}
	friend ostream& operator<< (ostream &out, const longlongplus &number) {
		out << to_string(number);
		return out;
	}
};