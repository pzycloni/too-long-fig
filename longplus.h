#include <string>

using namespace std;

const unsigned long long int LONG = 10e15;

class longplus {
	unsigned long long low, high;

public:
	longplus(unsigned long long number = 0) {
		low = number % LONG;
		high = number / LONG;
	}
	longplus(unsigned long long _low, unsigned long long _high) {
		low = _low;
		high = _high;
	}
	longplus operator+(const longplus &summand) const {
		return longplus((summand.low + low) % LONG, (summand.high + high) + (summand.low + low) / LONG);
	}
	friend string to_string(const longplus &lpnum) {
		return std::to_string(lpnum.high) + std::to_string(lpnum.low);
	}
};