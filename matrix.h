template <typename T> class matrix {
	T **arr;
	unsigned rowCount = 2;
	unsigned colCount = 2;

public:
	matrix(T **_initArr, unsigned rows, unsigned cols) {
		rowCount = rows;
		colCount = cols;
		arr = _initArr;
	}

	matrix(T first, T second, T third, T fourth) {
		arr = new T*[rowCount];
		for (unsigned row = 0; row < rowCount; row++)
			arr[row] = new T[colCount];

		arr[0][0] = first;
		arr[0][1] = second;
		arr[1][0] = third;
		arr[1][1] = fourth;
	}

	matrix(T first, T second, bool horizontal) {

		if (horizontal) {
			rowCount = 1;
			colCount = 2;

			arr = new T*[rowCount];
			for (unsigned row = 0; row < rowCount; row++)
				arr[row] = new T[colCount];

			arr[0][0] = first;
			arr[0][1] = second;
		}
		else {
			rowCount = 2;
			colCount = 2;

			arr = new T*[rowCount];
			for (unsigned row = 0; row < rowCount; row++)
				arr[row] = new T[colCount];

			arr[0][0] = first;
			arr[1][0] = second;
		}


	}

	matrix(unsigned rows, unsigned cols) {
		rowCount = rows;
		colCount = cols;
		arr = new T*[rows];
		for (unsigned row = 0; row < rows; row++)
			arr[row] = new T[cols];

		for (unsigned row = 0; row < rows; row++) {
			for (unsigned col = 0; col < cols; col++) {
				arr[row][col] = 0;
			}
		}
	}

	long long int getRows() const {
		return rowCount;
	}

	long long int getCols() const {
		return colCount;
	}

	T getElement(int row, int col) const {
		return this->arr[row][col];
	}

	matrix<T> operator+=(const matrix<T> &summand) const {
		for (unsigned row = 0; row < rowCount; row++) {
			for (unsigned col = 0; col < colCount; col++) {
				arr[row][col] = arr[row][col] + summand.arr[row][col];
			}
		}
		return *this;
	}

	matrix<T> operator+(const matrix<T> &summand) const {
		matrix<T> result(colCount, rowCount);
		for (unsigned row = 0; row < rowCount; row++) {
			for (unsigned col = 0; col < colCount; col++) {
				result.arr[row][col] = arr[row][col] + summand.arr[row][col];
			}
		}
		return result;
	}

	matrix<T> operator*(const matrix<T> &multiplicand) const {
		matrix<T> result(this->getRows(), multiplicand.getCols());

		for (unsigned row = 0; row < this->getRows(); row++) {
			for (unsigned col = 0; col < multiplicand.getCols(); col++) {
				for (unsigned k = 0; k < this->getCols(); k++) {
					result.arr[row][col] = result.arr[row][col] + this->arr[row][k] * multiplicand.arr[k][col];
				}
			}
		}

		return result;
	}

	matrix<T> operator*=(const matrix<T> &multiplicand) {
		matrix<T> result = (*this) * multiplicand;
		(*this) = result;
		return *this;
	}

	matrix<T> operator=(const matrix<T> &rhs) {
		colCount = rhs.getCols();
		rowCount = rhs.getRows();

		arr = new T*[rowCount];
		for (unsigned row = 0; row < rowCount; row++)
			arr[row] = new T[colCount];

		for (unsigned row = 0; row < rowCount; row++) {
			for (unsigned col = 0; col < colCount; col++) {
				arr[row][col] = rhs.arr[row][col];
			}
		}

		return *this;
	}

	const T operator()(const unsigned row, const unsigned col) const {
		return arr[row][col];
	}

	friend ostream& operator<< (ostream &out, const matrix<T> &rhs) {
		for (unsigned row = 0; row < rhs.getRows(); row++) {
			for (unsigned col = 0; col < rhs.getCols(); col++) {
				out << rhs.arr[row][col] << " ";
			}
			out << endl;
		}
		return out;
	}
};