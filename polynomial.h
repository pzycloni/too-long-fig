#pragma once
#include <vector>
#include <random>
#include <math.h>
#include "complexMatrix.h"

using namespace std;

template <typename T> class polynomial {

public:

	vector <T> coefficients;

	polynomial(const vector <T> &coefficients) {
		this->coefficients.resize(coefficients.size());
		for (int i = 0; i < this->coefficients.size(); i++) {
			this->coefficients[i] = coefficients[i];
		}
	}

	polynomial(int power) {
		this->coefficients.resize(power + 1);
	}

	polynomial() {}

	vector<T> getPolynomialCoef() {
		return this->coefficients;
	}


	/*
	** Производная многочлена
	*/
	polynomial<T> derivative() {

		if (this->coefficients.size() < 2) {
			return polynomial<T>(0);
		}

		vector <T> container;

		int cols = this->coefficients.size();

		for (int i = 0; i < cols - 1; i++) {
			auto coef = this->coefficients[cols - 1 - i];
			double power = cols - 1 - i;
			auto varible = coef * power;

			container.push_back(varible);
		}
		std::reverse(std::begin(container), std::end(container));
		return polynomial<T>(container);
	}

	/*
	** Производная многочлена n-ой степени
	*/
	polynomial<T> derivative(int n) {

		if (n == 0) {
			return *this;
		}

		auto container = this->coefficients;
		for (int i = 0; i < n; i++) {
			polynomial<T> p(container);
			polynomial<T> result = p.derivative();
			container = result.coefficients;
		}
		return polynomial<T>(container);
	}


	/*
	** Метод Ньютона
	*/
	T neuton(complex<double> someRoot) {
		auto derivativePolinomial = this->derivative();
		return someRoot - (*this)(someRoot) / derivativePolinomial(someRoot);
	}


	/*
	** Генератор матрицы с единицами под главной диаганалью
	** и нулями в остальных случаях
	*/
	complexMatrix generateMatrix(int rows, int cols) {
		complexMatrix result(rows, cols);
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				if (col == row - 1)
					result.matrix[row][col] = 1;
				else
					result.matrix[row][col] = 0;
			}
		}
		return result;
	}


	/*
	** Генератор случайного вектора
	*/
	complexMatrix generateVector(int cols) {
		default_random_engine generator;

		vector<vector<complex<double> > > randomVector;
		uniform_real_distribution<double> distributionVector(-1.0, 1.0);

		for (int i = 0; i < cols - 1; i++) {
			double randomNumberVectorReal = distributionVector(generator);
			double randomNumberVectorImg = distributionVector(generator);
			vector<complex<double> > temp;
			temp.push_back(complex<double>(randomNumberVectorReal, randomNumberVectorImg));
			randomVector.push_back(temp);
		}
		return complexMatrix(randomVector);
	}

	
	/*
	** Поиск корня многочлена
	*/
	T findRoot() {
		if (this->degree() == 1) {
			return -this->coefficients[0] / this->coefficients[1];
		}

		// нормализуем многочлен - делим на коэффициент при старшей степени
		auto a = this->coefficients[this->coefficients.size() - 1];
		for (int i = 0; i <= this->degree(); i++) {
			this->coefficients[i] /= a;
		}

		// сдвиг мн-на на случайное число, достаточно малое
		default_random_engine generator;
		uniform_real_distribution<double> distribution(-0.3, 0.3);
		auto randomNumberReal = distribution(generator);
		auto randomNumberImg = distribution(generator);
		complex<double> complexA(randomNumberReal, randomNumberImg);
		auto shiftPolynomial = this->shift(complexA);

		// для полученного многочлена формируем матрицу
		int rows = this->coefficients.size();
		int cols = this->coefficients.size();
		auto matrixA = this->generateMatrix(rows - 1, cols - 1);
		
		for (int i = 0; i < matrixA.getCols(); i++) {
			auto coef = shiftPolynomial.coefficients[cols - 2 - i];
			matrixA.matrix[0][i] = -coef;
		}

		// формируем случайный вектор (матрицу)
		complexMatrix randomVectorMatrix = this->generateVector(cols);
		
		// матрицу умножаем на случайный вектор
		complex<double> lambda(0.0, 0.0), prevLambda(1.0, 1.0);

		double epsilon = 0.01;
		int power = 1;
		bool isNan = false;

		while (abs(prevLambda - lambda) > epsilon) {
			prevLambda = lambda;
			// матрица в степени n
			int n = pow(2, power);
			complexMatrix m = matrixA^n;
			auto uv = m * randomVectorMatrix;
			lambda = uv.matrix[0][0] / uv.matrix[1][0];
			power++;
		}

		// уточняем корень с помощью метода Ньютона
		prevLambda = 1.0;
		epsilon = 1e-10;
		while (abs(prevLambda - lambda) > epsilon) {
			auto temp = lambda;
			lambda = this->neuton(lambda);
			prevLambda = temp;
		}
		return lambda;
	}
	

	/*
	** Нахождение всех корней
	** https://mathematics.ru/courses/algebra/content/chapter2/section1/paragraph5/theory.html#.XKr7M-szbOQ
	*/
	vector<T> findRoots() {
		vector<T> roots;

		auto container = this->coefficients;

		// делим пока не найдем все корни
		while (container.size() > 1) {
			polynomial<T> p(container);
			auto root = p.findRoot();
			roots.push_back(root);
			polynomial<T> result = p.div(root);
			container = result.coefficients;
		}

		return roots;
	}


	/*
	** Поиск корня, кратности корня многочлена
	*/
	vector<pair<int, T> > findDegreeMultipleRoot(polynomial<T> poly) {
		vector<pair<int, T> > roots;

		double epsilon = 1e-4;

		polynomial<T> p(poly.coefficients);
		// делим пока не найдем все корни
		while (p.degree() >= 1) {
			int multiple = 1;
			T root = p.findRoot();
			p = p.div(root);
			while (abs(p(root)) < epsilon && p.degree() > 0) {
				multiple++;
				p = p.div(root);
			}
			roots.push_back(make_pair(multiple, root));
		}
		return roots;
	}


	/*
	** Степень многочлена
	*/
	int degree() const {
		return this->coefficients.size() - 1;
	}


	/*
	** Деление многочлена
	*/
	polynomial<T> div(T a) {

		if (this->coefficients.size() < 2) {
			return this->coefficients;
		}

		polynomial<T> result(this->degree() - 1);
		result.coefficients.back() = this->coefficients.back();

		for (int power = this->degree() - 2; power > -1; power--) {
			result.coefficients[power] = a * result.coefficients[power + 1] + this->coefficients[power + 1];
		}
		return result;
	}


	/*
	** Сдвиг многочлена на коэффициент а
	*/
	polynomial<T> shift(T a) {
		vector <T> result;
		for (int iteration = 0; iteration < this->degree(); iteration++) {
			// полином полученный при делении
			auto poly = this->div(a);
			// остаток от деления
			auto remainder = poly(a);
			// собираем новый полином из остатков
			result.push_back(remainder);
		}
		result.push_back(this->coefficients[this->coefficients.size() - 1]);
		return polynomial<T>(result);
	}


	/*
	** Вычисление значения многочлена в точке
	*/
	T operator()(const T& value) const {
		T result = 0;
		for (int power = 0; power < this->coefficients.size(); power++) {
			result += this->coefficients[power] * pow(value, power);
		}
		return result;
	}


	/*
	** Доступ к значению коэффициента многочлена
	*/
	T operator[](const unsigned& power) const {
		return this->coefficients[this->coefficients.size() - power - 1];
	}

	friend ostream& operator<< (ostream &out, const polynomial<T> &rhs) {
		for (unsigned power = 0; power <= rhs.degree(); power++) {
			out << rhs.coefficients[power] << "*x^" << power << " ";
		}
		out << endl;
		return out;
	}
};