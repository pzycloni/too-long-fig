template <typename T> class fraction {

private:
	T numerator;
	T denominator;

public:

	fraction(T numerator, T denominator) {
		this->numerator = numerator;
		this->denominator = denominator;
	}

	fraction<T> operator*(const fraction<T> &multiplicand) const {
		fraction<T> result(this->numerator * multiplicand->numerator, this->denominator * multiplicand->denominator);
	}

	fraction<T> operator+(const fraction<T> &multiplicand) const {
		fraction<T> result(this->numerator + multiplicand->numerator, this->denominator + multiplicand->denominator);
	}

	T getNumerator() {
		return this->numerator;
	}

	T getDenominator() {
		return this->denominator;
	}
};